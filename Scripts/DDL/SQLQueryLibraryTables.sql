--Create database Library
USE Library
Go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create table Author(
	AuthorId bigint Identity(1,1) not null,
	Constraint PK_Author_AuthorId Primary key clustered (AuthorId asc) ,
	Name nvarchar(512) not null,
	SurName nvarchar(512) not null,
	LastName nvarchar(512) null,
	BirthDate date null,
	Country nvarchar(64) null
	)
	Go


Create table Book(
	BookId bigint Identity(1, 1) not null,
	Constraint PK_Book_BookId Primary key clustered (BookId asc),
	BookName nvarchar(512) not null,
	ReleaseYear int null,
	PublisherId bigint null,
	AuthorId bigint null,
	NumberOfPages int not null,
	TagId bigint null
	)
	Go


Create table BookLease(
	BookLeaseId bigint identity(1, 1) not null,
	Constraint PK_BookLease_BookLeaseId Primary key clustered (BookLeaseId asc),
	BookId bigint null,
	ReaderId bigint null,
	LeaseDate date not null,
	LeaseTime time null
	)
	Go


Create table BookMovement(
	BookMovementId bigint identity(1, 1) not null,
	Constraint PK_BookMovement_BookMovementId Primary key clustered (BookMovementId asc),
	BookId bigint null,
	OperationDate date not null,
	TypeOperationId bigint null,
	Number int not null
	)
	Go


Create table Publisher(
	PublisherId bigint identity(1, 1) not null,
	Constraint PK_Publisher_PublisherId Primary key clustered (PublisherId asc),
	Name nvarchar(512) not null,
	Address nvarchar(512) null,
	Phone nvarchar(64) null
	)
	Go
	

Create table Reader(
	ReaderId bigint identity(1, 1) not null,
	Constraint PK_Reader_ReaderId Primary key clustered (ReaderId asc),
	Name nvarchar(512) not null,
	SurName nvarchar(512) null,
	LastName nvarchar(512) null,
	LibraryCardNumber nvarchar not null,
	Address nvarchar(512) null,
	Phone nvarchar(64) null
	)
	Go


Create table Tag(
	TagId bigint identity(1, 1) not null,
	Constraint PK_Tag_TagId Primary key clustered (TagId asc),
	TagName nvarchar(512) not null,
	)
	Go


Create table TypeOperation(
	TypeOperationId bigint identity(1, 1) not null,
	Constraint PK_TypeOperation_TypeOperationId Primary key clustered (TypeOperationId asc),
	TypeOperationName nvarchar(512) not null,
	)
	Go