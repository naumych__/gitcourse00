USE [Library]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Author] FOREIGN KEY([AuthorId])
REFERENCES [dbo].[Author] ([AuthorId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Author]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Publisher] FOREIGN KEY([PublisherId])
REFERENCES [dbo].[Publisher] ([PublisherId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Publisher]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Tag] FOREIGN KEY([TagId])
REFERENCES [dbo].[Tag] ([TagId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Tag]
GO
ALTER TABLE [dbo].[BookLease]  WITH CHECK ADD  CONSTRAINT [FK_BookLease_Book] FOREIGN KEY([BookId])
REFERENCES [dbo].[Book] ([BookId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BookLease] CHECK CONSTRAINT [FK_BookLease_Book]
GO
ALTER TABLE [dbo].[BookLease]  WITH CHECK ADD  CONSTRAINT [FK_BookLease_Reader] FOREIGN KEY([ReaderId])
REFERENCES [dbo].[Reader] ([ReaderId])
GO
ALTER TABLE [dbo].[BookLease] CHECK CONSTRAINT [FK_BookLease_Reader]
GO
ALTER TABLE [dbo].[BookMovement]  WITH CHECK ADD  CONSTRAINT [FK_BookMovement_Book] FOREIGN KEY([BookId])
REFERENCES [dbo].[Book] ([BookId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BookMovement] CHECK CONSTRAINT [FK_BookMovement_Book]
GO
ALTER TABLE [dbo].[BookMovement]  WITH CHECK ADD  CONSTRAINT [FK_BookMovement_TypeOperation] FOREIGN KEY([TypeOperationId])
REFERENCES [dbo].[TypeOperation] ([TypeOperationId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BookMovement] CHECK CONSTRAINT [FK_BookMovement_TypeOperation]
GO
