Use Library
Go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--ALTER TABLE Book ADD  CONSTRAINT FK_Book_Author FOREIGN KEY(AuthorId)
--REFERENCES Author (AuthorId)
--On delete set null
--On update set null
--Go

ALTER TABLE Book ADD  CONSTRAINT FK_Book_Publisher FOREIGN KEY(PublisherId)
REFERENCES Publisher (PublisherId)
On delete set null
On update set null
Go

ALTER TABLE Book ADD CONSTRAINT FK_Book_Tag FOREIGN KEY(TagId)
REFERENCES Tag (TagId)
On delete set null
On update set null
Go

ALTER TABLE BookLease ADD CONSTRAINT FK_BookLease_Book FOREIGN KEY(BookId)
REFERENCES Book (BookId)
On delete set null
On update set null
Go

ALTER TABLE BookLease ADD CONSTRAINT FK_BookLease_Reader FOREIGN KEY(ReaderId)
REFERENCES Reader (ReaderId)
On delete set null
On update set null
Go

ALTER TABLE BookMovement ADD CONSTRAINT FK_BookMovement_Book FOREIGN KEY(BookId)
REFERENCES Book (BookId)
On delete set null
On update set null
Go


ALTER TABLE BookMovement ADD CONSTRAINT FK_BookMovement_TypeOperation FOREIGN KEY(TypeOperationId)
REFERENCES TypeOperation (TypeOperationId)
On delete set null
On update set null
Go